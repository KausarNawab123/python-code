import tkinter.messagebox as box
from tkinter.filedialog import asksaveasfile
import pandas as pd
import tkinter as tk
import win32api
import sqlite3
import tkinter.font as font
from tkinter import ttk, filedialog


# This is the class for my login page,
# I used DJ Oamen on youtube to create this class - How to create a login system
class Centre_Window:
    def __init__(self, master):
        self.master = master
        self.master.title("Login page")
        self.frame = tk.Frame(self.master, bg="pink")
        self.master.configure(bg="light pink")
        # This is me creating the labels
        self.stafflbl_font = font.Font(family='Georgia', size="27", weight="bold")
        self.stafflbl = tk.Label(self.frame, text='Staff Registration', font=self.stafflbl_font, bg='light pink',
                                 fg='dark blue')
        self.stafflbl.pack(pady=20)
        # This is the username label and packed it
        self.lbl_font = font.Font(family='Calibri', size="16", weight='bold')
        self.ULabel = tk.Label(self.frame, text="Username", font=self.lbl_font, fg='black', bg='light pink').pack()
        self.U = tk.StringVar()
        self.UEntry = tk.Entry(self.frame)
        self.UEntry.pack()
        # This is the password label and packed it
        self.PLabel = tk.Label(self.frame, text="Password", font=self.lbl_font, fg='black', bg='light pink').pack()
        self.P = tk.StringVar()
        self.PEntry = tk.Entry(self.frame, show='*')
        self.PEntry.pack()
        # This is the login button and exit button
        self.loginButton = tk.Button(self.frame, text="Login", command=self.message, bg='white')
        self.loginButton.pack(pady=20)
        self.exitBtn = tk.Button(self.frame, text="Exit", fg='black', bg='white', cursor='question_arrow', command=quit)
        self.exitBtn.pack()
        # This is the size of the GUI and also the placement of it
        width, height = 400, 400
        screen_width = self.master.winfo_screenwidth()
        screen_height = self.master.winfo_screenheight()
        x_cord = int((screen_width / 2) - (width / 2))
        y_cord = int((screen_height / 2) - (height / 2))
        self.master.geometry("{}x{}+{}+{}".format(width, height, x_cord, y_cord))

        self.frame.pack()

    # this is where the username an password gets accepted- multiple access login
    def message(self):
        U1 = self.UEntry.get()
        P1 = self.PEntry.get()
        if U1 == 'Kausar123' and P1 == 'Kausar':
            mainscreen()
        elif U1 == 'Sneha123' and P1 == 'Sneha':
            mainscreen()
        elif U1 == 'Asma345' and P1 == 'Asma345':
            mainscreen()
        elif U1 == '2' and P1 == '2':
            mainscreen()
        elif U1 == 'Vlad' and P1 == 'Vlad':
            mainscreen()
        elif U1 == 'Habiba123' and P1 == 'Habiba123':
            mainscreen()
        elif U1 == '3' and P1 == '3':
            mainscreen()
        else:
            box.showerror("message", 'Invalid login')


# This is the mainpage where it has the specific size and its in the centre
def mainscreen():
    main_screen = tk.Toplevel(root)
    width, height = 400, 400
    screen_width = main_screen.winfo_screenwidth()
    screen_height = main_screen.winfo_screenheight()
    x_cord = int((screen_width / 2) - (width / 2))
    y_cord = int((screen_height / 2) - (height / 2))
    main_screen.geometry("{}x{}+{}+{}".format(width, height, x_cord, y_cord))
    main_screen.title("Main Screen")
    main_screen.configure(bg="pink")

    screen_lbl_font = font.Font(family='Georgia', size="27", weight="bold")
    screen_lbl = tk.Label(main_screen, text='Main Page', font=screen_lbl_font, bg='pink', fg='dark blue')
    screen_lbl.pack(pady=10)

    # These are all my buttons where  each button has been assigned to a function
    Database_Button = tk.Button(main_screen, text="Queries and Reviews", command=Data, bg='white')
    Database_Button.pack(pady=20, ipadx=100)

    Toys_Button = tk.Button(main_screen, text="Discount Schemes", command=Discount_Scheme, bg='white')
    Toys_Button.pack(pady=15, ipadx=100)

    report_btn = tk.Button(main_screen, text="Sales Report", command=Report, bg="white")
    report_btn.pack(pady=15, ipadx=100)

    exit_btn = tk.Button(main_screen, text="Exit", command=root.destroy, bg="white")
    exit_btn.pack(pady=15, ipadx=100)


# This is the function where it responds to customer reviews
# I had to connect to sqlite and also add my directory and my access database
def Data():
    import pyodbc

    path = 'C:\\Users\\kausa\\OneDrive\\Documents\\kausars work\\COMP1811- PARADIGMS OF PROGRAMMING\\'
    connection_string = 'Driver={Microsoft Access Driver (*.mdb, *.accdb)};'
    connection_string += 'DBQ=' + path + 'New_Database_-_FINAL.accdb;'

    conn = pyodbc.connect(connection_string)
    cursor = conn.cursor()

    reviews = input("Open reviews? ")

    # This is my class, and i have an if statement where if i open reviews it allows to connect to my db
    # If i don't what to respond to my reviews then i say no and its closed
    def open_reviews():
        if reviews == "yes":
            cursor.execute('SELECT * FROM Reviews')

            for row in cursor.fetchall():
                print(row)

        elif reviews == "no":
            print("Reviews Closed")

        else:
            print("Database terminated")

    class Comments:
        def __init__(self, reviews):
            self.reviews = reviews

    result = Comments(reviews)
    open_reviews()

    #  RESPONDING TO THE REVIEWS

    print("")

    print("RESPONCE ID")
    responce_id = input()
    print("STAFF ID")
    staff_id = input()
    print("CUSTOMER ID")
    cust_id = input()
    print("CUSTOMER NAME")
    cust_name = input()
    print("CUSTOMER COMMENT")
    cust_comment = input()
    print("STAFF'S RESPONCE")
    staff_reponce = input()

    # This links to my database where it inserts all the information i had responded to in my database.
    cursor = conn.cursor()
    SQLCommand = (
        "INSERT INTO Responce(Responce_ID, Staff_ID, Customer_ID, Cust_Name, Cust_comment, Staff_responce) VALUES (?, ?, ?, ?, ?, ?)")
    Values = [responce_id, staff_id, cust_id, cust_name, cust_comment, staff_reponce]

    cursor.execute(SQLCommand, Values)

    conn.commit()


# This is the function where it add products and discounts to my database
# Below it is the database itself.
# Used codemy.com for the whole of this function

def Discount_Scheme():
    root = tk.Tk()
    root.title("Discount Scheme")
    root.geometry("600x500")
    root.configure(bg='light blue')

    width_2, height_2 = 600, 480
    screen_width_2 = root.winfo_screenwidth()
    screen_height_2 = root.winfo_screenheight()
    x_cord_2 = int((screen_width_2 / 2) - (width_2 / 2))
    y_cord_2 = int((screen_height_2 / 2) - (height_2 / 2))
    root.geometry("{}x{}+{}+{}".format(width_2, height_2, x_cord_2, y_cord_2))

    conn = sqlite3.connect('toys_data.db')
    c = conn.cursor()

    # c.execute("""CREATE TABLE toys_database (
    #         toy_name text,
    #         brand text,
    #         category text,
    #         quantity integer,
    #         price integer,
    #         discount integer
    #         )""")

    conn.commit()

    conn.close()

    def discount1():
        conn = sqlite3.connect('toys_data.db')
        c = conn.cursor()
        price = float(input('The original price: £'))
        discount = float(input('How much is the discount, %'))
        reduced_price = price - price * discount / 100
        print('£', reduced_price)

    # This is the show function which allows to show my categories
    # Used codemy.com for the whole of this function
    def show():
        conn_3 = sqlite3.connect('toys_data.db')
        c_3 = conn_3.cursor()
        c_3.execute("SELECT *, oid FROM toys_database")
        datas = c_3.fetchall()
        print_data = ''
        for data in datas:
            print_data += str(data[0]) + ": " + str(data[4]) + ": " + str(data[5]) + "\n"

        # When showing the category i had made it specific to what labels i want to show.
        class Categories:

            def __init__(self, root):
                self.category_lbl = tk.Label(root, text="Categories:  " + "\n" + "\n" + print_data, bg="lightblue")
                self.category_lbl.grid(row=13, column=0, columnspan=2)

        ca = Categories(root)

        conn_3.commit()
        conn_3.close()

    # I had created delete function which delete a category depending on what primary key you insert
    # Used codemy.com for the whole of this function
    def delete():
        conn_2 = sqlite3.connect('toys_data.db')
        c_2 = conn_2.cursor()
        c_2.execute("DELETE from toys_database WHERE oid = " + delete_box.get())
        delete_box.delete(0, tk.END)
        conn_2.commit()
        conn_2.close()

    # This is the add function which adds categories
    # Used codemy.com for the whole of this function
    def add():
        conn_1 = sqlite3.connect('toys_data.db')
        c_1 = conn_1.cursor()
        c_1.execute("INSERT INTO toys_database VALUES (:toy_name, :brand, :category, :quantity, :price, :discount)",
                    {
                        'toy_name': toy_name.get(),
                        'brand': brand.get(),
                        'category': category.get(),
                        'quantity': quantity.get(),
                        'price': price.get(),
                        'discount': discount.get()
                    })

        conn_1.commit()
        conn_1.close()

        toy_name.delete(0, tk.END)
        brand.delete(0, tk.END)
        category.delete(0, tk.END)
        quantity.delete(0, tk.END)
        price.delete(0, tk.END)
        discount.delete(0, tk.END)

    # This is the add function which edit categories
    # Made it specific to size and colour and also its in another mainscreen
    # Used codemy.com for the whole fo this function
    def edit():
        global editing
        editing = tk.Tk()
        editing.title("Edit Category")

        width, height = 320, 200
        screen_width = editing.winfo_screenwidth()
        screen_height = editing.winfo_screenheight()
        x_cord = int((screen_width / 2) - (width / 2))
        y_cord = int((screen_height / 2) - (height / 2))
        editing.geometry("{}x{}+{}+{}".format(width, height, x_cord, y_cord))

        editing.configure(bg="light yellow")
        conn_4 = sqlite3.connect('toys_data.db')
        c_4 = conn_4.cursor()
        data_id = delete_box.get()
        c_4.execute("SELECT * FROM toys_database WHERE oid = " + data_id)
        datas = c_4.fetchall()

        global toy_name_editing
        global brand_editing
        global category_editing
        global quantity_editing
        global price_editing
        global discount_editing

        # I had created labels for the edit category
        # Used codemy.com
        toy_name_editing = tk.Entry(editing, width=25)
        toy_name_editing.grid(row=0, column=1, padx=20, pady=(10, 0))
        brand_editing = tk.Entry(editing, width=25)
        brand_editing.grid(row=1, column=1)
        category_editing = tk.Entry(editing, width=25)
        category_editing.grid(row=2, column=1)
        quantity_editing = tk.Entry(editing, width=25)
        quantity_editing.grid(row=3, column=1)
        price_editing = tk.Entry(editing, width=25)
        price_editing.grid(row=4, column=1)
        discount_editing = tk.Entry(editing, width=25)
        discount_editing.grid(row=5, column=1)

        toy_name_lbl_editing = tk.Label(editing, text="Toy", bg="light yellow")
        toy_name_lbl_editing.grid(row=0, column=0, pady=(10, 0))
        brand_lbl_editing = tk.Label(editing, text="Brand", bg="light yellow")
        brand_lbl_editing.grid(row=1, column=0)
        category_lbl_editing = tk.Label(editing, text="Category", bg="light yellow")
        category_lbl_editing.grid(row=2, column=0)
        quantity_lbl_editing = tk.Label(editing, text="Quantity", bg="light yellow")
        quantity_lbl_editing.grid(row=3, column=0)
        price_lbl_editing = tk.Label(editing, text="Price", bg="light yellow")
        price_lbl_editing.grid(row=4, column=0)
        discount_lbl_editing = tk.Label(editing, text="Discount", bg="light yellow")
        discount_lbl_editing.grid(row=5, column=0)
        save_button = tk.Button(editing, text="Save", command=update, bg="white")
        save_button.grid(row=6, column=0, columnspan=2, pady=10, padx=10, ipadx=130)

        for data in datas:
            toy_name_editing.insert(0, data[0])
            brand_editing.insert(0, data[1])
            category_editing.insert(0, data[2])
            quantity_editing.insert(0, data[3])
            price_editing.insert(0, data[4])
            discount_editing.insert(0, data[5])

    # This is updating the edit category
    # Used codemy.com for the whole of this function
    def update():
        connect_5 = sqlite3.connect('toys_data.db')
        c_5 = connect_5.cursor()
        data_id = delete_box.get()
        c_5.execute("""UPDATE toys_database SET
               toy_name = :toy,
               brand = :brand,
               category = :category,
               quantity = :quantity,
               price = :price,
               discount = :discount

               WHERE oid = oid""",
                    {
                        'toy': toy_name_editing.get(),
                        'brand': brand_editing.get(),
                        'category': category_editing.get(),
                        'quantity': quantity_editing.get(),
                        'price': price_editing.get(),
                        'discount': discount_editing.get(),
                        'oid': data_id

                    })
        connect_5.commit()
        connect_5.close()
        editing.destroy()

    # I had created labels for the mainpage category
    # Used codemy.com
    toy_name = tk.Entry(root, width=25)
    toy_name.grid(row=0, column=1, padx=20, pady=(10, 0))
    brand = tk.Entry(root, width=25)
    brand.grid(row=1, column=1)
    category = tk.Entry(root, width=25)
    category.grid(row=2, column=1)
    quantity = tk.Entry(root, width=25)
    quantity.grid(row=3, column=1)
    price = tk.Entry(root, width=25)
    price.grid(row=4, column=1)
    discount = tk.Entry(root, width=25)
    discount.grid(row=5, column=1)
    delete_box = tk.Entry(root, width=45)
    delete_box.grid(row=9, column=1, pady=5)

    toy_name_lbl = tk.Label(root, text="Toy", bg="light blue")
    toy_name_lbl.grid(row=0, column=0, pady=(10, 0))
    brand_lbl = tk.Label(root, text="Brand", bg="light blue")
    brand_lbl.grid(row=1, column=0)
    category_lbl = tk.Label(root, text="Category", bg="light blue")
    category_lbl.grid(row=2, column=0)
    quantity_lbl = tk.Label(root, text="Quantity", bg="light blue")
    quantity_lbl.grid(row=3, column=0)
    price_lbl = tk.Label(root, text="Price", bg="light blue")
    price_lbl.grid(row=4, column=0)
    discount_lbl = tk.Label(root, text="Discount", bg="light blue")
    discount_lbl.grid(row=5, column=0)
    delete_box_lbl = tk.Label(root, text="ID", bg="light blue")
    delete_box_lbl.grid(row=9, column=0, pady=5)

    # This is all the buttons which are assigned to a specific function
    # Used codemy.com
    add_button = tk.Button(root, text="Add", bg="white", command=add)
    add_button.grid(row=6, column=0, columnspan=2, pady=10, padx=10, ipadx=200)

    show_button = tk.Button(root, text="Show", command=show, bg="white")
    show_button.grid(row=7, column=0, columnspan=2, pady=10, padx=10, ipadx=228)

    delete_button = tk.Button(root, text="Delete", command=delete, bg="white")
    delete_button.grid(row=10, column=0, columnspan=2, pady=10, padx=10, ipadx=228)

    edit_button = tk.Button(root, text="Edit", command=edit, bg="white")
    edit_button.grid(row=11, column=0, columnspan=2, pady=10, padx=10, ipadx=234)

    exit_button = tk.Button(root, text="Exit", command=root.destroy, bg="white")
    exit_button.grid(row=12, column=0, columnspan=2, pady=10, padx=10, ipadx=260)


# As you can see here, def report was the button in the mainscreen.
# Furthermore, I had made it according to color, added a title etc.
# Used codemy.com for the whole of this function
def Report():
    root = tk.Tk()
    root.title('Sales Report')
    root.geometry("900x700")
    root.configure(bg='light green')

    my_frame = tk.Frame(root)
    my_frame.pack(pady=20)

    my_tree = ttk.Treeview(my_frame)

    def file_open():
        global df
        filename = tk.filedialog.askopenfilename(
            initialdir="C:/Users/kausa/OneDrive/Documents/kausars work",
            title="Open A File",
            filetype=(("xlsx files", "*.xlsx"), ("All Files", "*.*"))
        )
        # I had also made a correction error where if they cannot open the file then it causes an error,
        # If the file is not found it causes an found error.
        # Used codemy.com
        if filename:
            try:
                filename = r"{}".format(filename)
                df = pd.read_excel(filename)
            except ValueError:
                my_label.config(text="Error Of Not Opening")
            except FileNotFoundError:
                my_label.config(text="Error Of Not Being Found")

        clear_tree()

        my_tree["column"] = list(df.columns)
        my_tree["show"] = "headings"

        for column in my_tree["column"]:
            my_tree.heading(column, text=column)

        df_rows = df.to_numpy().tolist()
        for row in df_rows:
            my_tree.insert("", "end", values=row)

        my_tree.pack()

    def clear_tree():
        my_tree.delete(*my_tree.get_children())

    # This is the function where it will generate the sales report and will allow me to save and print them.
    # Used codemy.com
    def print_file():
        # printer_name = win32print.GetDefaultPrinter
        file_to_print = filedialog.askopenfilename(initialdir="C://Users/kausa/OneDrive/Documents/kausars work",
                                                   title='open file',
                                                   filetypes=(("xlsx files", "*.xlsx"), ("All Files", "*.*")))

        if file_to_print:
            win32api.ShellExecute(0, "print", file_to_print, None, ".", 0)

    def save():
        files = [('All Files', '*.*'),
                 ('Python Files', '*.py'),
                 ('Text Document', '*.txt')]
        file = asksaveasfile(filetypes=files, defaultextension=files)

    # This is the menu page where it has the sales report spreadsheet.
    # Used codemy.com

    my_menu = tk.Menu(root)
    root.config(menu=my_menu)

    file_menu = tk.Menu(my_menu, tearoff=False)
    my_menu.add_cascade(label="Sales Report Spreadsheet", menu=file_menu)
    file_menu.add_command(label="Open", command=file_open)

    my_label = tk.Label(root, text='')
    my_label.pack(pady=20)

    # This is the print, save, and exit button which carry out the appropriate command.
    # Used codemy.com
    print_btn = tk.Button(root, text="Print File", command=print_file, bg="white")
    print_btn.pack(pady=35, ipadx=100)

    Save_btn = tk.Button(root, text='Save File', command=lambda: save(), bg='white')
    Save_btn.pack(pady=45, ipadx=100)

    exit_btn = tk.Button(root, text="Exit", command=root.destroy, bg="white")
    exit_btn.pack(pady=45, ipadx=100)


root = tk.Tk()
app = Centre_Window(root)
root.mainloop()
